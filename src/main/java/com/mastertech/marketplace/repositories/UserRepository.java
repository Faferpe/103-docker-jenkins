package com.mastertech.marketplace.repositories;

import org.springframework.data.repository.CrudRepository;

import com.mastertech.marketplace.models.User;

public interface UserRepository extends CrudRepository<User, Integer>{
	
}
